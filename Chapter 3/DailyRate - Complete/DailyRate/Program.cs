﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyRate
{
    class Program
    {
        static void Main(string[] args)
        {
            (new Program()).Run();
        }

        void Run()
        {
            double dailyRate = ReadDouble("Enter your daily rate: ");
            int noOfDays = ReadInt("Enter the number of days: ");
            WriteFee(CalculateFee(dailyRate, noOfDays));
        }

        private void WriteFee(double fee) => Console.WriteLine($"The consultant's fee is: {fee * 1.1}");

        private double CalculateFee(double dailyRate, int noOfDays) => dailyRate * noOfDays;

        private int ReadInt(string value)
        {
            Console.Write(value);
            string line = Console.ReadLine();
            return int.Parse(line);
        }

        private double ReadDouble(string value)
        {
            Console.Write(value);
            string line = Console.ReadLine();
            return double.Parse(line);
        }
    }
}
